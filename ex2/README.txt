203481544
*****
Question 5 comments:


First I used manhattan distance to approximate the distance between pacman and food position and to each state
I gave penalty if he was far from the closest food.
Then to each state I gave penalty for each food dot that left on the board.

I calculate the distance from each ghost and gave bonus if you can with manhatn distance approximation eat ghost from
that state if the ghost is scared, if not then I ignore it and also I gave bonus if pacman is at least 2 step
away from ghost.

Also I gave penalty for each capsule left on the board to encourage pacman to eat capsules.