# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).

  You do not need to change anything in this class, ever.
  """

    def getStartState(self):
        """
     Returns the start state for the search problem
     """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
       state: Search state

     Returns True if and only if the state is a valid goal state
     """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
       state: Search state

     For a given state, this should return a list of triples,
     (successor, action, stepCost), where 'successor' is a
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental
     cost of expanding to that successor
     """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
      actions: A list of actions to take

     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
  Search the deepest nodes in the search tree first [p 85].

  Your search algorithm needs to return a list of actions that reaches
  the goal.  Make sure to implement a graph search algorithm [Fig. 3.7].

  To get started, you might want to try some of these simple commands to
  understand the search problem that is being passed in:

  print("Start:", problem.getStartState())
  print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
  print("Start's successors:", problem.getSuccessors(problem.getStartState()))
  """

    def dfsGenerator(startState):

        stack = util.Stack()
        stack.push(StateWrapper(startState))

        while not stack.isEmpty():

            (previous_state, new_states) = yield stack.pop()

            if new_states:
                for val in new_states:
                    stack.push(StateWrapper(val[0]))

        yield None

    search = genericSearch(problem, dfsGenerator)
    return search


def breadthFirstSearch(problem):
    "Search the shallowest nodes in the search tree first. [p 81]"

    def bfsGenerator(startState):

        queue = util.Queue()
        queue.push(StateWrapper(startState))

        while not queue.isEmpty():

            (previous_state, new_states) = yield queue.pop()

            if new_states:
                for val in new_states:
                    queue.push(StateWrapper(val[0]))

        yield None

    search = genericSearch(problem, bfsGenerator)
    return search


def uniformCostSearch(problem):
    "Search the node of least total cost first. "

    def ufsGenerator(startState):

        queue = util.PriorityQueue()
        queue.push(StateWrapper(startState), 0)

        while not queue.isEmpty():

            (previous_state, new_states) = yield queue.pop()

            if new_states:
                for val in new_states:
                    state_cost = previous_state.getCost() + val[2]
                    queue.push(StateWrapper(val[0], state_cost), state_cost)

        yield None

    search = genericSearch(problem, ufsGenerator)
    return search


def nullHeuristic(state, problem=None):
    """
  A heuristic function estimates the cost from the current state to the nearest
  goal in the provided SearchProblem.  This heuristic is trivial.
  """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    def astrGenerator(startState):

        queue = util.PriorityQueue()
        queue.push(StateWrapper(startState), 0)

        while not queue.isEmpty():

            (previous_state, new_states) = yield queue.pop()

            if new_states:
                for val in new_states:
                    state = val[0]
                    cost_state = previous_state.getCost() + val[2] # g(n)
                    predicted_cost = cost_state + heuristic(state, problem)
                    queue.push(StateWrapper(state, cost_state), predicted_cost)

        yield None

    search = genericSearch(problem, astrGenerator)
    return search


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch


def genericSearch(problem, fringe_impletation):

    def get_path(state, predecessors):

        path = []

        cur = predecessors[state]
        while cur[0] != problem.getStartState():
            path.insert(0, cur[1])
            cur = predecessors[cur[0]]

        path.insert(0, cur[1])
        return path

    gen = fringe_impletation(problem.getStartState())

    predecessors = {}
    closed = set()

    # get start state
    state = gen.__next__()

    while state:

        cur_state = state.getState()

        if problem.isGoalState(cur_state):
            return get_path(cur_state, predecessors)

        if cur_state not in closed:
            successors = [(successor, (cur_state, direction, cost))
                          for (successor, direction, cost) in problem.getSuccessors(cur_state)
                          if successor not in closed]
            predecessors.update(successors)
            closed.add(cur_state)
        else:
            successors = []

        state = gen.send((state, ((successor, direction, cost)
                                  for (successor, (predecessor, direction, cost)) in successors)))

    return []


class StateWrapper:
    def __init__(self, state, cost=0):
        self.state = state
        self.cost = cost

    def getCost(self):
        return self.cost

    def getState(self):
        return self.state
