# multiAgents.py 
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import random
import math

from util import manhattanDistance
from game import Directions
import util
from game import Agent

INFTY = float("inf")


class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """

    def getAction(self, gameState):
        """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (oldFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        oldFood = currentGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        evaluation = 0
        if successorGameState.isWin():
            return INFTY
        if successorGameState.isLose():
            return 0

        # don't allow stopping in place
        if action == Directions.STOP:
            return 0

        food_list = oldFood.asList()
        food_distance = [manhattanDistance(food, newPos) for food in food_list]
        min_food_distance = min(food_distance)
        evaluation += 100 / (min_food_distance + 1)

        ghosts_position = successorGameState.getGhostPositions()

        bad = []
        good = []

        for i in range(len(ghosts_position)):
            distance = manhattanDistance(ghosts_position[i], newPos)
            scared_time = newScaredTimes[i]
            if 0 < scared_time < distance:
                good.append(distance)
            else:
                bad.append(distance)

        if good:
            evaluation += 50

        if bad:
            min_ghost_distance = min(bad)
            evaluation += min_ghost_distance

        capsules = successorGameState.getCapsules()
        if newPos in capsules:
            evaluation += 100

        return evaluation


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
    ans = currentGameState.getScore()
    # print(ans)
    return ans


class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
  """

    def getAction(self, gameState):
        """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """

        best = -INFTY
        best_action = Directions.STOP
        legal_actions = gameState.getLegalActions(0)

        for action in legal_actions:

            if action == Directions.STOP:
                continue

            # assumption pacman will be playing at least against one
            score = self.minimax_search(gameState.generateSuccessor(0, action), self.depth, 1)

            if score > best:
                best = score
                best_action = action

        return best_action

    def minimax_search(self, game_state, depth, agent):

        # if terminal state
        if depth == 0 or game_state.isWin() or game_state.isLose():
            return self.evaluationFunction(game_state)

        if agent == 0:
            function = max
        else:
            function = min

        num_agents = game_state.getNumAgents()
        next_agent = agent + 1
        next_depth = depth - math.floor(next_agent / num_agents)
        next_agent = next_agent % num_agents

        legal_actions = game_state.getLegalActions(agent)

        scores = []
        for action in legal_actions:

            # remove stop from pacman agent possibilities
            if agent != 0 or action != Directions.STOP:
                search = self.minimax_search(game_state.generateSuccessor(agent, action), next_depth, next_agent)
                scores.append(search)

        return function(scores)


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
  """

    def getAction(self, gameState):
        """
      Returns the minimax action using self.depth and self.evaluationFunction
    """

        best = -INFTY
        best_action = Directions.STOP
        legal_actions = gameState.getLegalActions(0)

        alpha = -INFTY

        for action in legal_actions:

            if action == Directions.STOP:
                continue

            # assumption pacman will be playing at least against one
            v = self.alpha_beta_pruning_search(gameState.generateSuccessor(0, action), self.depth, alpha, INFTY, 1)

            if v > alpha:
                alpha = v
                best_action = action

        return best_action

    def alpha_beta_pruning_search(self, game_state, depth, alpha, beta, agent):

        # if terminal state
        if depth == 0 or game_state.isWin() or game_state.isLose():
            return self.evaluationFunction(game_state)

        if agent == 0:
            function = max
        else:
            function = min

        num_agents = game_state.getNumAgents()
        next_agent = agent + 1
        next_depth = depth - math.floor(next_agent / num_agents)
        next_agent = next_agent % num_agents

        legal_actions = game_state.getLegalActions(agent)

        for action in legal_actions:

            if agent != 0 or action != Directions.STOP:

                search = self.alpha_beta_pruning_search(game_state.generateSuccessor(agent, action), next_depth, alpha,
                                                        beta, next_agent)

                # update alpha/beta
                if agent == 0:
                    alpha = function(alpha, search)
                else:
                    beta = function(beta, search)

                # prune branch if there is need
                if beta <= alpha:
                    break

        if agent == 0:
            return alpha
        else:
            return beta


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
    Your expectimax agent (question 4)
  """

    def getAction(self, gameState):
        """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
        legal_actions = gameState.getLegalActions(0)

        alpha = -INFTY

        best_actions = []

        for action in legal_actions:

            if action == Directions.STOP:
                continue

            # assumption pacman will be playing at least against one
            successor = gameState.generateSuccessor(0, action)
            v = self.expectimax_search(successor, self.depth, 1)

            if v > alpha:
                alpha = v
                best_actions = [action]
            elif v == alpha:
                best_actions.append(action)

        chosen_action = random.choice(best_actions)  # Pick randomly among the best

        return chosen_action

    def expectimax_search(self, game_state, depth, agent):

        # if terminal state
        if depth == 0 or game_state.isWin() or game_state.isLose():
            return self.evaluationFunction(game_state)

        num_agents = game_state.getNumAgents()

        if agent == 0:
            function = max
        else:
            # calculate
            function = lambda x: sum(x) / len(x)

        next_agent = agent + 1
        next_depth = depth - math.floor(next_agent / num_agents)
        next_agent = next_agent % num_agents

        legal_actions = game_state.getLegalActions(agent)

        scores = []
        for action in legal_actions:
            if agent != 0 or action != Directions.STOP:
                score = self.expectimax_search(game_state.generateSuccessor(agent, action), next_depth, next_agent)
                scores.append(score)

        return function(scores)


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    In readme
  """

    if currentGameState.isWin():
        return INFTY

    if currentGameState.isLose():
        return -INFTY

    pacman_position = currentGameState.getPacmanPosition()

    food_list = currentGameState.getFood().asList()
    food_distance = [(manhattanDistance(food, pacman_position), food) for food in food_list]
    min_food_distance = min(food_distance, key=lambda x: x[0])

    food_val = -min_food_distance[0]
    food_val -= len(food_list) * 10

    ghost_states = currentGameState.getGhostStates()
    ghost_position = [ghost.getPosition() for ghost in ghost_states]
    scared_times = [ghost.scaredTimer for ghost in ghost_states]
    bad = []
    good = []

    for i in range(len(ghost_position)):
        distance = manhattanDistance(ghost_position[i], pacman_position)
        scared_time = scared_times[i]
        if 0 < scared_time < distance:
            good.append(distance)
        elif scared_time <= 0:
            bad.append(distance)

    v = 0
    if good:
        v += 1

    v2 = 0
    if bad:
        min_ghost_distance = min(bad)
        v2 += min_ghost_distance

    ghost_val = min(v2, 2) * 10 + v * 50

    evaluation = food_val + ghost_val - len(currentGameState.getCapsules()) * 100

    return evaluation


# Abbreviation
better = betterEvaluationFunction


class ContestAgent(MultiAgentSearchAgent):
    """
    Your agent for the mini-contest
  """

    def getAction(self, gameState):
        """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """

        util.raiseNotDefined()


def real_distance():
    pass
