from itertools import combinations_with_replacement, permutations, product


def createDomainFile(domainFileName, n):
    domainFile = open(domainFileName, 'w')  # use domainFile.write(str) to write to domainFile

    numbers = list(range(n))  # [0,...,n-1]
    pegs = {'a', 'b', 'c'}
    moves = [p for p in permutations(pegs, 2)]

    domainFile.write("Propositions:")

    domainFile.write("\n")

    sep = ' '

    domainFile.write(sep.join(["{0}{1}".format(e1, e2) for e1, e2 in product(pegs, numbers)]))

    domainFile.write("\n")

    domainFile.write("Actions:")
    domainFile.write("\n")

    for number in numbers:
        for move in moves:
            domainFile.write("Name: m{0}t{1}{2}".format(number, move[0], move[1]))
            domainFile.write("\n")

            other_peg = next(iter(set(pegs) - set(move)))

            pre = ["{0}{1}".format(move[0], number)]

            for cr in range(0, number):
                pre.append("{0}{1}".format(other_peg, cr))

            domainFile.write("Pre: {0}".format(' '.join(pre)))
            domainFile.write("\n")

            domainFile.write("Add: {0}{1}".format(move[1], number))
            domainFile.write("\n")

            domainFile.write("Del: {0}{1}".format(move[0], number))
            domainFile.write("\n")

    domainFile.close()


def createProblemFile(problemFileName, n):
    numbers = list(range(n))  # [0,...,n-1]
    pegs = ['a', 'b', 'c']
    problemFile = open(problemFileName, 'w')  # use problemFile.write(str) to write to problemFile

    problemFile.write("Initial State: {0}".format(" ".join(['a'+str(i) for i in numbers])))
    problemFile.write('\n')

    problemFile.write("Goal State: {0}".format(" ".join(['c'+str(i) for i in numbers])))
    problemFile.write('\n')

    problemFile.close()


import sys

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: hanoi.py n')
        sys.exit(2)

    n = int(float(sys.argv[1]))  # number of disks
    domainFileName = 'hanoi' + str(n) + 'Domain.txt'
    problemFileName = 'hanoi' + str(n) + 'Problem.txt'

    createDomainFile(domainFileName, n)
    createProblemFile(problemFileName, n)
